Physics engine developed during my Masters.


Main collision detection  algorithms contained within BoundingVolume.h/.cpp

Main collision resolution algorithms contained within Collider.h/.cpp component

Main physics/movement code is in RigidBody.h/.cpp

Spatial partition grid increases broad efficiency, found in SpatialPartitionGrid.h/.cpp

Graphics.h/.cpp is an interface to a graphics library, this version uses OpenGL

JSON is used to load in simulation parameters, JSON code in JsonFactory and JsonLoadable

Math helper classes are Matrix2/3 and Vector2/3

Main classes main, Simulation and SimObject, all component inherited classes.


Largely influenced by Unity.