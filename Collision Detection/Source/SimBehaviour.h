// Jake

#pragma once

#ifndef SIM_BEHAVIOUR
#define SIM_BEHAVIOUR

class SimBehaviour
{
public:
	virtual void Start() = 0;
	virtual void Update() = 0;
	virtual void LateUpdate() = 0;
	virtual void Render( float t ) = 0;
};

#endif /* SIM_BEHAVIOUR */