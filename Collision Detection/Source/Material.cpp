#include "Material.h"

Material::Material()
{

}

Material::Material( float _r, float _g, float _b, float _a ) : r{ _r }, g{ _g }, b{ _b }, a{ _a }
{

}

bool Material::Load( const rapidjson::Value& v )
{
	if ( !v.HasMember( "Material" ) )
		return false;

	const rapidjson::Value& m = v["Material"];

	r = m["R"].GetDouble();
	g = m["G"].GetDouble();
	b = m["B"].GetDouble();
	a = m["A"].GetDouble();

	return true;
}