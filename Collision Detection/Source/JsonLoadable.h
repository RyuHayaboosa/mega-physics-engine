// Jake

#pragma once

#ifndef JSON_LOADABLE_H
#define JSON_LOADABLE_H

#include "rapidjson/document.h"

class JsonLoadable
{
public:
	virtual bool Load( const rapidjson::Value& v ) = 0;
};

#endif /* JSON_LOADABLE_H */