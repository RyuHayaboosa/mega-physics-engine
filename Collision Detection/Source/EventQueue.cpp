#include "EventQueue.h"

std::vector<void( *)( void *pData )> EventQueue::mCallbacks[Event::last];

void EventQueue::Notify( Event e, void *pData )
{
	for ( auto callback : mCallbacks[e] )
	{
		callback( pData );
	}
}

void EventQueue::Listen( Event e, void( *callback)( void *pData ) )
{
	mCallbacks[e].push_back( callback );
}