// Jake

#pragma once

#ifndef PLANE_H
#define PLANE_H

#include "BoundingVolume.h"
#include "Vector2.h"

class Plane : public BoundingVolume
{
public:
	explicit Plane( const Vector2 &n = { 1.0f, 0.0f }, float d = 0.0f );

	Vector2 ClosestPointOnPlane( const Vector2 &p );

	// inherit from bounding volume
	Vector2 GetTranslation() const;
	float GetRotation() const;
	Vector2 GetScale() const;
	
	void SetTranslation( const Vector2 &t );
	void SetRotation( float r );
	void SetScale( const Vector2 &s );

	void Transform( const Vector2 &t, float r, const Vector2 &s );

	// variables
	Vector2 normal;
	float distance;
};

#endif /* PLANE_H */