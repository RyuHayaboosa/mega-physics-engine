#include "Frustum.h"

#include "gl/glut.h"

Frustum::Frustum( float range, int width, int height, int depth ) : mRange{ range }, mWidth{ width }, mHeight{ height }, mDepth{ depth }
{

}

void Frustum::Update( int w, int h )
{	
	mWidth	= w;
	mHeight	= h;

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	
	if ( mWidth > mHeight )
	{
		mRight		= mRange * mWidth / mHeight;
		mDown		= mRange;
		mForward	= mRange * 2.0f;
	}
	else
	{
		mRight		= mRange;
		mDown		= mRange * mHeight / mWidth;
		mForward	= mRange * 2.0f;
	}
	
	mLeft	= -mRight;
	mUp		= -mDown;
	mBack	= -mForward;

	glOrtho( mLeft, mRight, mDown, mUp, mBack, mForward );
}