// Jake

#pragma once

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "Component.h"
#include "Vector2.h"

class Transform : public Component
{
public:
	explicit Transform( const Vector2& _position = { 0.0f, 0.0f }, float _rotation = 0.0f, const Vector2& _scale = { 1.0f, 1.0f } );

	Vector2 position;
	float rotation;
	Vector2 scale;

	bool Load( const rapidjson::Value& v );
};

#endif /* TRANSFORM_H */