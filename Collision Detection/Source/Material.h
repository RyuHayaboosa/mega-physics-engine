// Jake

#pragma once

#ifndef MATERIAL_H
#define MATERIAL_H

#include "Component.h"

class Material : public Component
{
public:
	Material();
	explicit Material( float _r, float _g, float _b, float _a );

	float r, g, b, a;

	bool Load( const rapidjson::Value& v );
};

#endif /* MATERIAL_H */