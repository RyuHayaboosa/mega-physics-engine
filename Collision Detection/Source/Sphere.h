// Jake

#pragma once

#ifndef SPHERE_H
#define SPHERE_H

#include "BoundingVolume.h"
#include "Vector2.h"

class Sphere : public BoundingVolume
{
public:
	explicit Sphere( const Vector2 &c = { 0.0f, 0.0f }, float r = 1.0f );
	
	// inherit from bounding volume
	Vector2 GetTranslation() const;
	float GetRotation() const;
	Vector2 GetScale() const;
	
	void SetTranslation( const Vector2 &t );
	void SetRotation( float r );
	void SetScale( const Vector2 &s );

	void Transform( const Vector2 &t, float r, const Vector2 &s );

	// variables
	Vector2 centre;
	float radius;
};

#endif /* SPHERE_H */