#include "Collider.h"

#include "AABB.h"
#include "Plane.h"
#include "Sphere.h"

#include "RigidBody.h"
#include "Transform.h"

#include "Manifold.h"
#include "SimObject.h"
#include "Vector3.h"

#include "EventQueue.h"
#include "Graphics.h"
#include "Simulation.h"
#include "SpatialPartitionGrid.h"

Collider::~Collider()
{
	delete mBoundingVolume;
	delete mOldBoundingVolume;
}

void Collider::Start()
{
	UpdateBoundingVolume();

	SpatialPartitionGrid::PlaceSimObject( *mSimObject );
}

void Collider::Update()
{
	UpdateBoundingVolume();

	// check for collisions
	for ( SimObject *simObject : Simulation::GetSimObjects() )
	{
		// stop at self, each pair only gets checked once
		if ( simObject == mSimObject ) break;

		// if sim objects have colliders, are in the same layer and are adjacent
		if ( simObject->collider && mLayer != simObject->collider->mLayer && SpatialPartitionGrid::DetectAdjacency( *simObject, *mSimObject ) )
		{
			Manifold m;
			if ( BoundingVolume::DetectIntersection( mBoundingVolume, simObject->collider->mBoundingVolume, m ) )
			{
				// tell simulation there was a collision
				EventQueue::Notify( Event::collision, nullptr );

				// resolve
				ResolveIntersection( *simObject, *mSimObject, m );
			}
		}
	}
}

void Collider::LateUpdate()
{
	SpatialPartitionGrid::PlaceSimObject( *mSimObject );
}

void Collider::Render( float t )
{
	// interpolate between previous and current frame
	Vector2 position	= (mBoundingVolume->GetTranslation()	* t) + ( mOldBoundingVolume->GetTranslation()	* (1.0f - t));
	float rotation		= (mBoundingVolume->GetRotation()		* t) + ( mOldBoundingVolume->GetRotation()		* (1.0f - t));
	Vector2 scale		= (mBoundingVolume->GetScale()			* t) + ( mOldBoundingVolume->GetScale()			* (1.0f - t));

	// render debug mesh
	switch ( mBoundingVolume->topology )
	{
	case BoundingVolume::aabb:
		Graphics::DrawCube( position, rotation, scale, { 0.5f, 1.0f, 0.5f }, true );
		break;
	case BoundingVolume::sphere:
		Graphics::DrawSphere( position, rotation, scale, { 0.5f, 1.0f, 0.5f }, true );
		break;
	case BoundingVolume::plane:
		Graphics::DrawPlane( position, rotation, scale, { 0.5f, 1.0f, 0.5f }, true );
		break;
	}
}

void Collider::ResolveIntersection( const SimObject &a, const SimObject &b, const Manifold &m ) const
{
	RigidBody *pAb = a.rigidBody;
	RigidBody *pBb = b.rigidBody;
	
	// stop if neither sim object have rigid bodies
	if ( !pAb && !pBb ) return;

	/*
		Data
	*/

	float restitution		= 1.0f;
	float totalInverseMass	= 0.0f;
	Vector2 velocity;

	if ( pAb )
	{
		restitution			= pAb->GetRestitution();
		velocity			= pAb->GetVelocity();
		totalInverseMass	= pAb->GetInverseMass();
	}

	if ( pBb )
	{
		restitution			= std::fminf( restitution, pBb->GetRestitution() );
		velocity			-= pBb->GetVelocity();
		totalInverseMass	+= pBb->GetInverseMass();
	}
	
	/*
		Impulse
	*/

	float velocityAlongNormal = Vector2::Dot( velocity, m.normal );
	
	if ( velocityAlongNormal > 0.0f )
		return;

	float magnitude = ( 1.0f + restitution ) * velocityAlongNormal;
	magnitude /= totalInverseMass;

	Vector2 impulse = m.normal * magnitude;

	// apply impulse
	if ( pAb ) pAb->AddVelocity( -( impulse * pAb->GetInverseMass() ) );
	if ( pBb ) pBb->AddVelocity( +( impulse * pBb->GetInverseMass() ) );

	/*
		Positional Correctness
	*/
	
	Vector2 pc = m.normal * Simulation::GetPCPercent() * ( std::fmaxf( m.penetration - Simulation::GetPCSlop(), 0.0f ) / totalInverseMass );
	
	// apply position correctness
	if ( pAb ) a.transform->position += pc * pAb->GetInverseMass();
	if ( pBb ) b.transform->position -= pc * pBb->GetInverseMass();

	/*
		Friction
	*/
	
	// re calculate velocity
	if ( pAb ) velocity = pAb->GetVelocity();
	if ( pBb ) velocity -= pBb->GetVelocity();
	
	Vector2 tangent = velocity - m.normal * velocityAlongNormal;
	tangent.Normalise();

	float friction = Vector2::Dot( velocity, tangent );
	friction /= totalInverseMass;

	Vector2 frictionImpulse;
	if ( std::abs( friction ) < -magnitude * 0.4f )
		frictionImpulse = tangent * friction;	// static
	else
		frictionImpulse = tangent * -magnitude * 0.2f;	// dynamic
	
	if ( pAb ) pAb->AddVelocity( -( frictionImpulse * pAb->GetInverseMass() ) );
	if ( pBb ) pBb->AddVelocity( +( frictionImpulse * pBb->GetInverseMass() ) );
}

void Collider::UpdateBoundingVolume()
{
	// update collider transform
	Transform &t = *mSimObject->transform;

	// if we're on an even frame
	if ( mOddFrame )
	{
		// calculate on old
		mOldBoundingVolume->Transform( t.position, t.rotation, t.scale );

		// then swap pointers
		BoundingVolume *tmp = mOldBoundingVolume;
		mOldBoundingVolume	= mBoundingVolume;
		mBoundingVolume		= mOldBoundingVolume;
	}
	else
	{
		// normal calculation
		mBoundingVolume->Transform( t.position, t.rotation, t.scale );
	}
	
	// flip bool
	mOddFrame = !mOddFrame;
}

bool Collider::Load( const rapidjson::Value& v )
{
	if ( !v.HasMember( "Collider" ) )
		return false;

	const rapidjson::Value &c = v["Collider"];

	std::string type = c["Type"].GetString();

	if ( type == "AABB" )
	{
		mOldBoundingVolume	= new AABB;
		mBoundingVolume		= new AABB;
	}
	else if ( type == "Sphere" )
	{
		mOldBoundingVolume	= new Sphere;
		mBoundingVolume		= new Sphere;
	}
	else if ( type == "Plane" )
	{
		mOldBoundingVolume	= new Plane;
		mBoundingVolume		= new Plane;
	}

	mLayer = c["Layer"].GetInt();

	return true;
}