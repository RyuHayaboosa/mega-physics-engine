// Jake

#pragma once

#ifndef FRUSTUM_H
#define FRUSTUM_H

class Frustum
{
public:
	explicit Frustum( float range = 0.0f, int width = 0, int height = 0, int depth = 0 );

	void SetRange( float range )	{ mRange = range; }
	void SetWidth( int width )		{ mWidth = width; }
	void SetHeight( int height )	{ mHeight = height; }
	void SetDepth( int depth )		{ mDepth = depth; }

	float GetRange() const		{ return mRange; }

	float GetLeft() const		{ return mLeft; }
	float GetRight() const		{ return mRight; }
	float GetDown() const		{ return mDown; }
	float GetUp() const			{ return mUp; }
	float GetBack() const		{ return mBack; }
	float GetForward() const	{ return mForward; }

	int GetWidth() const		{ return mWidth; }
	int GetHeight() const		{ return mHeight; }
	int GetDepth() const		{ return mDepth; }

	void Update( int w, int h );

private:
	float	mRange		= 0.0f;

	float	mLeft		= 0.0f,
			mRight		= 0.0f, 
			mDown		= 0.0f, 
			mUp			= 0.0f, 
			mBack		= 0.0f, 
			mForward	= 0.0f;

	int		mWidth		= 0, 
			mHeight		= 0,
			mDepth		= 0;
};

#endif /* FRUSTUM_H */