// Jake

#pragma once

#ifndef BOUNDING_VOLUME_H
#define BOUNDING_VOLUME_H

struct Manifold;

class Vector2;

class BoundingVolume
{
public:
	enum Topology { aabb, plane, sphere, last };

public:
	explicit BoundingVolume( Topology t );

	static bool DetectIntersection( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m );

	// get transforms, different for each child
	virtual Vector2 GetTranslation() const = 0;
	virtual float GetRotation() const = 0;
	virtual Vector2 GetScale() const = 0;
	
	// set transforms, different for each child
	virtual void SetTranslation( const Vector2 &t ) = 0;
	virtual void SetRotation( float r ) = 0;
	virtual void SetScale( const Vector2 &s ) = 0;

	virtual void Transform( const Vector2 &t, float r, const Vector2 &s ) = 0;
	
	const Topology topology;

private:
	static bool AABB_AABB		( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m );	// aabb vs aabb
	static bool AABB_Plane		( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// aabb vs plane
	static bool AABB_Sphere		( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// aabb vs sphere
	
	static bool Plane_AABB		( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// plane vs aabb
	static bool Plane_Plane		( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// plane vs plane
	static bool Plane_Sphere	( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// plane vs sphere
	
	static bool Sphere_AABB		( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// sphere vs aabb
	static bool Sphere_Plane	( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// sphere vs plane
	static bool Sphere_Sphere	( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  );	// sphere vs sphere
	
	static bool( *const callbacks[Topology::last][Topology::last] )( const BoundingVolume *const, const BoundingVolume *const, Manifold &m  );
};

#endif /* BOUNDING_VOLUME_H */