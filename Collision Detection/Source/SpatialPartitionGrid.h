// Jake

#pragma once

#ifndef SPATIAL_PARTITION_GRID_H
#define SPATIAL_PARTITION_GRID_H

#include "Vector2.h"

class SimObject;

class SpatialPartitionGrid
{
public:
	// constructor
	static void Init();

	// places sim object in partition
	static bool PlaceSimObject( SimObject &so );

	// detects if two sim objects are adjacent
	static bool DetectAdjacency( const SimObject &a, const SimObject &b );

	// render grid
	static void Render();

private:
	static const int Width = 4, Height = 4;
	
	struct Partition
	{
		int id; Vector2 min; Vector2 max;
	};

	static Partition mPartitions[Width][Height];
};

#endif /* SPATIAL_PARTITION_H */