#include "AABB.h"

AABB::AABB( const Vector2 &_min, const Vector2 &_max ) : BoundingVolume{ BoundingVolume::aabb }, min{ _min }, max{ _max }, mInitialMin{ _min }, mInitialMax{ _max }
{

}

bool AABB::ContainsPoint( const Vector2 &p ) const
{
	return ( p.x > min.x && p.x < max.x ) && ( p.y > min.y && p.y < max.y );
}

Vector2 AABB::Extents() const
{
	return ( max - min ) * 0.5f;
}

Vector2 AABB::GetTranslation() const
{
	return min + ( max - min ) * 0.5f;
}

float AABB::GetRotation() const
{
	return 0.0f;
}

Vector2 AABB::GetScale() const
{
	return max - min;
}

void AABB::SetTranslation( const Vector2 &t )
{
	min += t;
	max += t;
}

void AABB::SetRotation( float r )
{
	// aabbs dont rotate
}

void AABB::SetScale( const Vector2 &s )
{
	min = mInitialMin * s;
	max = mInitialMax * s;
}

void AABB::Transform( const Vector2 &t, float r, const Vector2 &s )
{
	SetScale( s );
	SetTranslation( t );
}