#include "Sphere.h"

#include <cmath>

Sphere::Sphere( const Vector2 &c, float r ) : BoundingVolume{ BoundingVolume::sphere }, centre{ c }, radius{ r }
{

}

Vector2 Sphere::GetTranslation() const
{
	return{ centre.x, centre.y };
}

float Sphere::GetRotation() const
{
	return 0.0f;
}

Vector2 Sphere::GetScale() const
{
	return{ radius, radius };
}

void Sphere::SetTranslation( const Vector2 &t )
{
	centre = t;
}

void Sphere::SetRotation( float r )
{
	// nothing to do here, spheres dont rotate really
}

void Sphere::SetScale( const Vector2 &s )
{
	// scale by largest element, ensure simObject is bound
	radius = std::fmaxf( s.x, s.y );
}

void Sphere::Transform( const Vector2 &t, float r, const Vector2 &s )
{
	SetTranslation( t );
	SetScale( s );
}