// Jake

#pragma once

#ifndef SIM_PARAMETERS_H
#define SIM_PARAMETERS_H

struct SimParameters
{
	int windowWidth;
	int windowHeight;

	float speed;

	float pcPercent;
	float pcSlop;
};

#endif /* SIMULATION_PARAMETERS_H */