// Jake

#pragma once

#ifndef COLLIDER_H
#define COLLIDER_H

#include "Component.h"

struct Manifold;

class BoundingVolume;

class Collider : public Component
{
public:	
	~Collider();

	// this
	const BoundingVolume *const GetBoundingVolume() const { return mBoundingVolume; }

	// Component
	void Start();
	void Update();
	void LateUpdate();
	void Render( float t );

	// JsonLoadable
	bool Load( const rapidjson::Value &v );

private:
	void ResolveIntersection( const SimObject &a, const SimObject &b, const Manifold &m ) const;
	void UpdateBoundingVolume();
	
	BoundingVolume *mBoundingVolume;
	BoundingVolume *mOldBoundingVolume;

	int mLayer;

	bool mOddFrame;
};

#endif /* COLLIDER_H */