// Jake

#pragma once

#ifndef SIMULATION_H
#define SIMULATION_H

#include <vector>

struct SimParameters;

class SimObject;
class Timer;

class Simulation
{
public:
	// constructor / deconstructor
	static void Init( int argc, char *argv[], SimParameters *simParameters, std::vector<SimObject *> *simObjects );
	static void Stop();

	// callbacks
	static void GameLoop();
	static void Resize( int w, int h );

	// simulation behaviour
	static void Start();
	static void Update();
	static void Render( float t );
	
	// getters
	static const std::vector<SimObject *>	&GetSimObjects()	{ return *mSimObjects; }
	static const Timer						&GetTimer()			{ return *mTimer; }
	static const Timer						&GetFrameTimer()	{ return *mFrameTimer; }
	
	static float GetPCPercent() { return mPcPercent; }
	static float GetPCSlop()	{ return mPcSlop; }

private:
	static std::vector<SimObject *> *mSimObjects;	// simulation objects
	static Timer					*mTimer;		// simulation timer
	static Timer					*mFrameTimer;	// frame timer

	static bool mInitialised;						// simulation has been initialised

	static float mSpeed;							// simulation speed
		
	static float mPcPercent;						// positional correctness percent
	static float mPcSlop;							// positional correctness slop
};

#endif /* SIMULATION_H */