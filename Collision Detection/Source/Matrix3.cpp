#include "Matrix3.h"

Matrix3::Matrix3( float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz ) : x{ xx, xy, xz }, y{ yx, yy, yz }, z{ zx, zy, zz }
{

}

Matrix3::Matrix3( const Vector3 &_x, const Vector3 &_y, const Vector3 &_z ) : Matrix3{ _x.x, _x.y, _x.z, _y.x, _y.y, _y.z, _z.x, _z.y, _z.z }
{

}

Vector3 &Matrix3::operator [] ( int i )
{
	switch ( i )
	{
	case 0: return x;
	case 1: return y;
	case 2: return z;

	default:
		throw "Matrix3::operator[int] subscript out of range.";
	}
}

const Vector3 &Matrix3::operator [] ( int i ) const
{
	switch ( i )
	{
	case 0: return x;
	case 1: return y;
	case 2: return z;

	default:
		throw "Matrix3::operator[int] (const) subscript out of range.";
	}
}