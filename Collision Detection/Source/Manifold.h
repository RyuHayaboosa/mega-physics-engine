// Jake

#pragma once

#ifndef MANIFOLD_H
#define MANIFOLD_H

#include "Vector2.h"

struct Manifold
{
	Vector2 normal;
	float penetration;
};

#endif /* MANIFOLD_H */