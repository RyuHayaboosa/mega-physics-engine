#include "Vector2.h"

#include <cmath>

#include "Vector3.h"

Vector2::Vector2()
{

}

Vector2::Vector2( float _x, float _y ) : x{ _x }, y{ _y }
{

}

Vector2::Vector2( const Vector3 &v ) : Vector2{ v.x, v.y }
{

}

Vector2 &Vector2::operator = ( const Vector2 &val )
{
	x = val.x;
	y = val.y;
	return *this;
}

Vector2 &Vector2::operator += ( const Vector2 &val )
{
	x += val.x;
	y += val.y;
	return *this;
}

Vector2 &Vector2::operator -= ( const Vector2 &val )
{
	x -= val.x;
	y -= val.y;
	return *this;
}

Vector2 &Vector2::operator *= ( const Vector2 &val )
{
	x *= val.x;
	y *= val.y;
	return *this;
}

Vector2 Vector2::operator + () const
{
	return Vector2{ x, y };
}

Vector2 Vector2::operator - () const
{
	return Vector2{ -x, -y };
}

Vector2 Vector2::operator + ( const Vector2 &val ) const
{
	return Vector2{ x + val.x, y + val.y };
}

Vector2 Vector2::operator - ( const Vector2 &val ) const
{
	return Vector2{ x - val.x, y - val.y };
}

Vector2 Vector2::operator * ( const Vector2 &val ) const
{
	return Vector2{ x * val.x, y * val.y };
}

Vector2 Vector2::operator / ( float val ) const
{
	return Vector2{ x / val, y / val };
}

Vector2 Vector2::operator * ( float val ) const
{
	return Vector2{ x * val, y * val };
}

float &Vector2::operator [] ( int i )
{
	switch ( i )
	{
	case 0:	return x;
	case 1: return y;

	default: 
		throw "Vector2::operator[int] subscript out of range.";
	}
}

const float &Vector2::operator [] ( int i ) const
{
	switch ( i )
	{
	case 0:	return x;
	case 1: return y;

	default: 
		throw "Vector2::operator[int] (const) subscript out of range.";
	}
}

void Vector2::Abs()
{
	x = std::abs( x );
	y = std::abs( y );
}

void Vector2::Clamp( const Vector2 &min, const Vector2 &max )
{
	x = std::fminf( std::fmaxf( x, min.x ), max.x );
	y = std::fminf( std::fmaxf( y, min.y ), max.y );
}

void Vector2::Inverse()
{
	x = 1.0f / x;
	y = 1.0f / y;
}

float Vector2::Length() const
{
	return std::sqrtf( ( x * x ) + ( y * y ) );
}

float Vector2::LengthSq() const
{
	return ( x * x ) + ( y * y );
}

void Vector2::Normalise()
{
	float length = Length();

	x /= length;
	y /= length;
}

void Vector2::Perpendicular()
{
	float f = x;

	x = -y;
	y = f;
}

void Vector2::Zero()
{
	x = y = 0.0f;
}

Vector2 Vector2::Clamp( const Vector2 &val, const Vector2 &min, const Vector2 &max )
{
	Vector2 retval{ val };
	retval.Clamp( min, max );
	return retval;
}

Vector2 Vector2::Abs( const Vector2 &a )
{
	Vector2 b{ a };
	b.Abs();
	return b;
}

float Vector2::Dot( const Vector2& a, const Vector2& b )
{
	return ( a.x * b.x ) + ( a.y * b.y );
}