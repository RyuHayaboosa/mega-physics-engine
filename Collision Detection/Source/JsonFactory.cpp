#include "JsonFactory.h"

#include <iostream>

#include "EventQueue.h"
#include "File.h"
#include "SimObject.h"
#include "SimParameters.h"

#include "Collider.h"
#include "Material.h"
#include "Renderer.h"
#include "Shape.h"
#include "StartingForce.h"
#include "Transform.h"

std::string			JsonFactory::mFilename;
std::string			JsonFactory::mFile;
rapidjson::Document JsonFactory::mDocument;

void JsonFactory::Parse( const char *const filename )
{
	mFilename	= filename;
	mFile		= File::Read( filename );
	mDocument.Parse( mFile.c_str() );
	
	std::cout << ( mDocument.IsNull() ? "Errors in JSON file.\n" : "No errors in JSON file.\n" );
}

std::vector<SimObject *> *JsonFactory::CreateSimObjects()
{
	// retval
	std::vector<SimObject *> *simObjects = new std::vector<SimObject *>();
	
	// simulation object values
	const rapidjson::Value &v = mDocument["SimObjects"];

	// number of simulation objects
	int length = mDocument["SimObjects"].Size();

	EventQueue::Notify( Event::sim_objects_loaded, &length );

	// for all
	for ( int i = 0; i < length; i++ )
	{
		const rapidjson::Value& vi = v[i];
		
		SimObject* so = new SimObject( 
			vi.HasMember( "Meta" )					? vi["Meta"].GetInt()						: -1,
			vi.HasMember("UseSpatialPartitioning")	? vi["UseSpatialPartitioning"].GetBool()	: true
		);

		simObjects->push_back( so );
		
		// Update
		AddComponent<RigidBody>		( v, *simObjects, i );
		AddComponent<Collider>		( v, *simObjects, i );

		// No Update
		AddComponent<Renderer>		( v, *simObjects, i );
		AddComponent<Transform>		( v, *simObjects, i );
		AddComponent<Material>		( v, *simObjects, i );
		AddComponent<Shape>			( v, *simObjects, i );

		// Custom
		AddComponent<StartingForce>	( v, *simObjects, i );
	}

	return simObjects;
}

SimParameters *JsonFactory::CreateSimParameters()
{
	// simulation object values
	const rapidjson::Value &v = mDocument["Simulation"];

	SimParameters *p = new SimParameters;

	p->windowWidth	= v["Width"].GetInt();
	p->windowHeight = v["Height"].GetInt();
	p->speed		= v["Speed"].GetDouble();
	p->pcPercent	= v["PCPercent"].GetDouble();
	p->pcSlop		= v["PCSlop"].GetDouble();

	return p;
}