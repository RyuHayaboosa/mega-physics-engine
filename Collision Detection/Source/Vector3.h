// Jake

#pragma once

#ifndef VECTOR_3_H
#define VECTOR_3_H

class Vector3
{
public:
	Vector3();
	Vector3( float _x, float _y, float _z );
	
	// Altering functions
	Vector3 &operator = ( const Vector3 &val );

	// Non-altering functions
	Vector3 operator + ( const Vector3 &val ) const;
	Vector3 operator - ( const Vector3 &val ) const;
	Vector3 operator * ( float ) const;
	
	float &operator [] ( int i );
	const float &operator [] ( int i ) const;

	// Help
	void Inverse();
	float Length() const;
	void Normalise();
	void Zero();

	// Static help
	static float Dot( const Vector3 &a, const Vector3 &b );
	static Vector3 Cross( const Vector3 &a, const Vector3 &b );
	
	// variables
	float x = 0.0f, y = 0.0f, z = 0.0f;
};

#endif /* VECTOR_3_H */