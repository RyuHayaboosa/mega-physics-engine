#include "Simulation.h"

#include "EventQueue.h"
#include "Graphics.h"
#include "SimObject.h"
#include "SimParameters.h"
#include "SpatialPartitionGrid.h"
#include "Timer.h"

std::vector<SimObject *>	*Simulation::mSimObjects;
Timer						*Simulation::mTimer;
Timer						*Simulation::mFrameTimer;

bool Simulation::mInitialised = false;

float Simulation::mSpeed;

float Simulation::mPcPercent;
float Simulation::mPcSlop;

void Simulation::Init( int argc, char *argv[], SimParameters *simParameters, std::vector<SimObject *> *simObjects )
{
	if ( mInitialised )
	{
		throw "Simulation::Init() called twice.\n";
	}

	// inst
	mSimObjects	= simObjects;
	mTimer		= new Timer;
	mFrameTimer = new Timer;

	// start timers
	mTimer->Start();
	mFrameTimer->Start();

	// init graphics
	Graphics::Init( argc, argv, simParameters->windowWidth, simParameters->windowHeight, GameLoop, Resize, argv[0] );

	// init spatial partition grid
	SpatialPartitionGrid::Init();

	// copy parameters
	mSpeed			= simParameters->speed;
	mPcPercent		= simParameters->pcPercent;
	mPcSlop			= simParameters->pcSlop;

	delete simParameters;

	mInitialised = true;
}

void Simulation::Stop()
{
	delete mSimObjects;
	delete mTimer;
	delete mFrameTimer;

	Graphics::Stop();
}

void Simulation::GameLoop()
{
	// Lag behind real time
	static double lag = 0.0;

	// Increase lag
	lag += mFrameTimer->ElapsedTime() * mSpeed;

	mFrameTimer->Reset();

	// While we are behind real time
	while ( lag > Timer::MsPerUpdate )
	{
		Update();

		EventQueue::Notify( Event::frame_completed, nullptr );

		// Decrease total lag
		lag -= Timer::MsPerUpdate;
	}

	// render time through current frame
	Render( lag / Timer::MsPerUpdate );
}

void Simulation::Resize( int w, int h )
{
	// Dont divide by 0
	if ( w == 0 ) w = 1;
	if ( h == 0 ) h = 1;
	
	Graphics::Resize( w, h );
}

void Simulation::Start()
{
	// start sim objects
	for ( auto so : *mSimObjects )
		so->Start();

	// start graphics
	Graphics::Start();
}

void Simulation::Update()
{
	// update sim objects
	for ( auto so : *mSimObjects )
		so->Update();

	// late update sim objects, always after update
	for ( auto so : *mSimObjects )
		so->LateUpdate();
}

void Simulation::Render( float t )
{
	// start framebuffer
	Graphics::BeginDraw();

	// render sim objects
	for ( auto so : *mSimObjects )
		so->Render( t );

	// render grid
	SpatialPartitionGrid::Render();

	// present framebuffer
	Graphics::EndDraw();
}