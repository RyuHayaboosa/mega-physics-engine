// Jake

#pragma once

#ifndef RUNTIME_DATA_H
#define RUNTIME_DATA_H

class RuntimeData
{
public:
	static void Init();
	static void SaveToFile( const char *const path );

private:
	static void OnSimObjectsLoaded( void *pData );
	static void OnCollision( void *pData );
	static void OnSimObjectVelocityUpdated( void *pData );
	static void OnFrameCompleted( void *pData );

	static float mSimulationEnergy;
};

#endif /* RUNTIME_DATA_H */