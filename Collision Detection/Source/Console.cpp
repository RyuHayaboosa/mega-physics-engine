#include "Console.h"

#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <windows.h>

void Console::Init()
{
	//Create a console for this application
	AllocConsole();

	//Redirect unbuffered STDOUT to the console
	HANDLE ConsoleOutput = GetStdHandle( STD_OUTPUT_HANDLE );
	int SystemOutput = _open_osfhandle( intptr_t( ConsoleOutput ), _O_TEXT );
	FILE *COutputHandle = _fdopen( SystemOutput, "w" );
	*stdout = *COutputHandle;
	setvbuf( stdout, NULL, _IONBF, 0 );

	//Redirect unbuffered STDERR to the console 
	HANDLE ConsoleError = GetStdHandle( STD_ERROR_HANDLE );
	int SystemError = _open_osfhandle( intptr_t( ConsoleError ), _O_TEXT );
	FILE *CErrorHandle = _fdopen( SystemError, "w" );
	*stderr = *CErrorHandle;
	setvbuf( stderr, NULL, _IONBF, 0 );

	//Redirect unbuffered STDIN to the console
	HANDLE ConsoleInput = GetStdHandle( STD_INPUT_HANDLE );
	int SystemInput = _open_osfhandle( intptr_t( ConsoleInput ), _O_TEXT );
	FILE *CInputHandle = _fdopen( SystemInput, "r" );
	*stdin = *CInputHandle;
	setvbuf( stdin, NULL, _IONBF, 0 );

	//make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog point to console as well
	std::ios::sync_with_stdio( true );
}

void Console::Unin()
{
	//Write "Press any key to exit"
	HANDLE ConsoleOutput = GetStdHandle( STD_OUTPUT_HANDLE );
	DWORD CharsWritten;
	WriteConsole( ConsoleOutput, "\nPress any key to exit", 22, &CharsWritten, 0 );

	//Disable line-based input mode so we can get a single character
	HANDLE ConsoleInput = GetStdHandle( STD_INPUT_HANDLE );
	SetConsoleMode( ConsoleInput, 0 );

	//Read a single character
	TCHAR InputBuffer;
	DWORD CharsRead;
	ReadConsole( ConsoleInput, &InputBuffer, 1, &CharsRead, 0 );
}

void Console::ClearScreen()
{
	COORD coordScreen = { 0, 0 };
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;
	HANDLE hConsole = GetStdHandle( STD_OUTPUT_HANDLE );

	GetConsoleScreenBufferInfo( hConsole, &csbi );
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	FillConsoleOutputCharacter( hConsole, TEXT( ' ' ), dwConSize, coordScreen, &cCharsWritten );
	GetConsoleScreenBufferInfo( hConsole, &csbi );
	FillConsoleOutputAttribute( hConsole, csbi.wAttributes, dwConSize, coordScreen, &cCharsWritten );
	SetConsoleCursorPosition( hConsole, coordScreen );
}

void Console::GotoXY( int x, int y )
{
	COORD point;
	point.X = x; point.Y = y;
	SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), point );
}

void Console::SetColour( Colour c )
{
	SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_INTENSITY | c );
}

void Console::Print( const char *const message )
{
	std::cout << message;
}

void Console::Print( const char *const message, int x, int y )
{
	GotoXY( x, y );
	Print( message );
}

void Console::Print( const char *const message, Colour c )
{
	SetColour( c );
	Print( message );
}

void Console::Print( const char *const message, int x, int y, Colour c )
{
	GotoXY( x, y );
	SetColour( c );
	Print( message );
}