// Jake

#pragma once

#ifndef TIMER_H
#define TIMER_H

#include <chrono>

class Timer
{
public:
	Timer();

	void Start();
	void Reset();
	double ElapsedTime() const;

	static const double MsPerUpdate;

private:
	std::chrono::steady_clock::time_point mStart;
};

#endif /* TIMER_H */