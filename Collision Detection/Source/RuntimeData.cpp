#include "RuntimeData.h"

#include <sstream>

#include "Console.h"
#include "EventQueue.h"

float RuntimeData::mSimulationEnergy;

void RuntimeData::Init()
{
//	EventQueue::Listen( Event::sim_objects_loaded,			OnSimObjectsLoaded );
//	EventQueue::Listen( Event::collision,					OnCollision );
//	EventQueue::Listen( Event::sim_object_velocity_updated, OnSimObjectVelocityUpdated );
//	EventQueue::Listen( Event::frame_completed,				OnFrameCompleted );
}

void RuntimeData::SaveToFile( const char *const path )
{

}

void RuntimeData::OnSimObjectsLoaded( void *pData )
{
	std::stringstream ss;
	ss << "Simulation Objects: " << *(int *)pData;

	Console::Print( ss.str().c_str(), 0, 3 );
}

void RuntimeData::OnCollision( void *pData )
{
	static int collisions;

	collisions++;
	
	std::stringstream ss;
	ss << "Collisions: " << collisions;

	Console::Print( ss.str().c_str(), 0, 5 );
}

void RuntimeData::OnSimObjectVelocityUpdated( void *pData )
{
	mSimulationEnergy += *(float *)pData;
}

void RuntimeData::OnFrameCompleted( void *pData )
{
	std::stringstream ss;
	ss << "Simulation Energy: " << mSimulationEnergy;

	Console::Print( ss.str().c_str(), 0, 4 );

	mSimulationEnergy = 0.0f;
}