#include "BoundingVolume.h"

#include <cmath>

#include "AABB.h"
#include "Manifold.h"
#include "Plane.h"
#include "Sphere.h"

bool( *const BoundingVolume::callbacks[Topology::last][Topology::last] )( const BoundingVolume *const, const BoundingVolume *const, Manifold &m  ) = 
{
	{ 
		BoundingVolume::AABB_AABB,	
		BoundingVolume::AABB_Plane,		
		BoundingVolume::AABB_Sphere		
	},

	{
		BoundingVolume::Plane_AABB,
		BoundingVolume::Plane_Plane,
		BoundingVolume::Plane_Sphere	
	},

	{
		BoundingVolume::Sphere_AABB,
		BoundingVolume::Sphere_Plane,
		BoundingVolume::Sphere_Sphere	
	}
};

BoundingVolume::BoundingVolume( Topology t ) : topology{ t }
{

}

bool BoundingVolume::DetectIntersection( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m )
{
	return callbacks[a->topology][b->topology]( a, b, m );
}

#pragma region AABB

bool BoundingVolume::AABB_AABB( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	AABB &aabbA = *(AABB *)a;
	AABB &aabbB = *(AABB *)b;

	Vector2 to = aabbA.GetTranslation() - aabbB.GetTranslation();
	
	Vector2 aExtent = aabbA.Extents();
	Vector2 bExtent = aabbB.Extents();

	Vector2 overlap = aExtent + bExtent - Vector2::Abs( to );
	
	if ( overlap.x > 0.0f && overlap.y > 0.0f )
	{
		if ( overlap.x < overlap.y )
		{
			m.penetration = overlap.x;

			if ( to.x < 0.0f )	m.normal = { +1.0f, 0.0f };
			else				m.normal = { -1.0f, 0.0f };
		}
		else
		{
			m.penetration = overlap.y;

			if ( to.y < 0.0f )	m.normal = { 0.0f, +1.0f };
			else				m.normal = { 0.0f, -1.0f };
		}

		return true;
	}

	return false;
}

// Assumes axis-aligned plane :(
bool BoundingVolume::AABB_Plane( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	AABB &aabb = *(AABB *)a;
	Plane &plane = *(Plane *)b;

	Vector2 point = plane.ClosestPointOnPlane( aabb.GetTranslation() );

	if ( aabb.ContainsPoint( point ) )
	{
		m.normal = point - aabb.GetTranslation();
		m.normal.Normalise();
		
		m.penetration = std::abs( point.x - aabb.min.x );
		m.penetration = std::fminf( m.penetration, std::abs( point.x - aabb.max.x ) );
		m.penetration = std::fminf( m.penetration, std::abs( point.y - aabb.min.y ) );
		m.penetration = std::fminf( m.penetration, std::abs( point.y - aabb.max.y ) );

		return true;
	}

	return false;
}

bool BoundingVolume::AABB_Sphere( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	AABB &aabb = *(AABB *)a;
	Sphere &sphere = *(Sphere *)b;

	// sphere to aabb
	Vector2 to = sphere.GetTranslation() - aabb.GetTranslation();

	// distance from sphere to aabb extent
	float distance = ( to - Vector2::Clamp( to, -aabb.Extents(), aabb.Extents() ) ).LengthSq();

	// stop if distance < sphere radius
	if ( distance > sphere.radius * sphere.radius ) return false;

	distance = std::sqrtf( distance ); // delayed sqrt

	// collision
	m.penetration = sphere.radius - distance;
	m.normal = to;
	m.normal.Normalise();
	return true;
}

#pragma endregion

#pragma region Plane

bool BoundingVolume::Plane_AABB( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	bool passed = AABB_Plane( b, a, m );

	// reverse normal
	m.normal = -m.normal;

	return passed;
}

bool BoundingVolume::Plane_Plane( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	// to do

	return false;
}

bool BoundingVolume::Plane_Sphere( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	Plane &plane = *(Plane *)a;
	Sphere &sphere = *(Sphere *)b;

	Vector2 point = plane.ClosestPointOnPlane( sphere.centre );

	// normal from collision point to sphere centre
	m.normal = sphere.centre - point;

	// penetration of collision point into sphere radius
	m.penetration = sphere.radius - m.normal.Length();

	// normals get normalised
	m.normal.Normalise();

	// true if penetration has value
	return m.penetration > 0.0f;
}

#pragma endregion

#pragma region Sphere

bool BoundingVolume::Sphere_AABB( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	bool passed = AABB_Sphere( b, a, m );

	// reverse normal
	m.normal = -m.normal;

	return passed;
}

bool BoundingVolume::Sphere_Plane( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	bool passed = Plane_Sphere( b, a, m );

	// reverse normal
	m.normal = -m.normal;

	return passed;
}

bool BoundingVolume::Sphere_Sphere( const BoundingVolume *const a, const BoundingVolume *const b, Manifold &m  )
{
	Sphere &sphereA = *(Sphere *)a;
	Sphere &sphereB = *(Sphere *)b;

	// normal from sphere centres
	m.normal = sphereB.centre - sphereA.centre;

	// penetration of sphere radii
	m.penetration = ( sphereA.radius + sphereB.radius ) - m.normal.Length();

	// normals get normalised
	m.normal.Normalise();

	// true if penetration has value
	return m.penetration > 0.0f;
}

#pragma endregion