// Jake

#pragma once

#ifndef RENDERER_H
#define RENDERER_H

#include "Component.h"
#include "Transform.h"

class Renderer : public Component
{
public:
	void Render( float t );

	bool Load( const rapidjson::Value& v );

private:
	Transform oldTransform;
};

#endif /* RENDERER_H */