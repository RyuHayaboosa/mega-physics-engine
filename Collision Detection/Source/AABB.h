// Jake

#pragma once

#ifndef AABB_H
#define AABB_H

#include "BoundingVolume.h"
#include "Vector2.h"

class AABB : public BoundingVolume
{
public:
	explicit AABB( const Vector2 &_min = { -0.5f, -0.5f }, const Vector2 &_max = { 0.5f, 0.5f } );
	
	bool ContainsPoint( const Vector2 &p ) const;
	Vector2 Extents() const;

	// inherit from bounding volume
	Vector2 GetTranslation() const;
	float GetRotation() const;
	Vector2 GetScale() const;
	
	void SetTranslation( const Vector2 &t );
	void SetRotation( float r );
	void SetScale( const Vector2 &s );

	void Transform( const Vector2 &t, float r, const Vector2 &s );

	// variables
	Vector2 min;
	Vector2 max;

private:
	const Vector2 mInitialMin;
	const Vector2 mInitialMax;
};

#endif /* AABB_H */