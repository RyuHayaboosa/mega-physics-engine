#include "Graphics.h"

#include "Frustum.h"
#include "gl/glut.h"
#include "Vector2.h"
#include "Vector3.h"

Frustum *Graphics::mFrustum;

void Graphics::Init( int argc, char *argv[], int width, int height, void( *display )( ), void( *resize )( int, int ), const char *const title )
{
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glViewport( 0, 0, width, height );

	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitWindowSize( width, height );
	glutCreateWindow( title );
	glutDisplayFunc( display );
	glutReshapeFunc( resize );

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_POLYGON_OFFSET_FILL );
	glPolygonOffset( 16.0f, 0.0f ); // Arbitrary numbers are arbitrary

	mFrustum = new Frustum{ 100.0f, width, height, 0 };
}

void Graphics::Stop()
{
	glDisable( GL_DEPTH_TEST );
	glDisable( GL_POLYGON_OFFSET_FILL );
}

void Graphics::Resize( int w, int h )
{
	mFrustum->Update( w, h );

	glViewport( 0, 0, w, h );
}

void Graphics::Start()
{
	glutMainLoop();
}

void Graphics::BeginDraw()
{
	// Clear screen
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void Graphics::EndDraw()
{
	// Swap back buffers
	glutSwapBuffers();

	// Loop
	glutPostRedisplay();
}

void Graphics::DrawCube( const Vector2 &t, float r, const Vector2 &s, const Vector3 &c, bool wireframe )
{
	// gl
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	// transform
	glTranslatef( t.x, t.y, 0.0f );
	glRotatef( r, 0.0f, 0.0f, 1.0f );
	glScalef( s.x, s.y, 1.0f );

	// material
	glColor3f( c.x, c.y, c.z );
	
	if ( wireframe )
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	}
	else
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	}

	// draw
	glutSolidCube( 1.0f );
}

void Graphics::DrawSphere( const Vector2 &t, float r, const Vector2 &s, const Vector3 &c, bool wireframe )
{
	// gl
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	// transform
	glTranslatef( t.x, t.y, 0.0f );
	glRotatef( r, 0.0f, 0.0f, 1.0f );
	glScalef( s.x, s.y, 1.0f );

	// material
	glColor3f( c.x, c.y, c.z );
	
	if ( wireframe )
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	}
	else
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	}

	// draw
	glutSolidSphere( 1.0f, 16, 2 );
}

void Graphics::DrawPlane( const Vector2 &t, float r, const Vector2 &s, const Vector3 &c, bool wireframe )
{
	// gl
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	// transform
	glTranslatef( t.x, t.y, 0.0f );
	glRotatef( r, 0.0f, 0.0f, 1.0f );
	glScalef( 0.1f, 1000.0f, 1000.0f );

	// material
	glColor3f( c.x, c.y, c.z );
	
	if ( wireframe )
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	}
	else
	{
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	}

	// draw
	glutSolidCube( 1.0f );
}

int Graphics::GetWindowWidth()
{
	return mFrustum->GetWidth();
}

int Graphics::GetWindowHeight()
{
	return mFrustum->GetHeight();
}

float Graphics::GetRange()
{
	return mFrustum->GetRange();
}