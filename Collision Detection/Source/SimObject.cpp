#include "SimObject.h"

#include "Component.h"

SimObject::SimObject( int meta, bool useSpatialPartitioning ) : mMeta{ meta }, mUseSpatialPartitioning{ useSpatialPartitioning }
{

}

void SimObject::Start()
{
	collider	= HasComponent<Collider>()	? &GetComponent<Collider>()		: nullptr;
	rigidBody	= HasComponent<RigidBody>() ? &GetComponent<RigidBody>()	: nullptr;
	transform	= HasComponent<Transform>() ? &GetComponent<Transform>()	: nullptr;

	for ( auto a : mComponents )
		a.second->Start();
}

void SimObject::Update()
{
	for ( auto a : mComponents )
		a.second->Update();
}

void SimObject::LateUpdate()
{
	for ( auto a : mComponents )
		a.second->LateUpdate();
}

void SimObject::Render( float t )
{
	for ( auto a : mComponents ) 
		a.second->Render( t );
}