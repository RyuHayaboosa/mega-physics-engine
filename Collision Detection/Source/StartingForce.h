// Jake

#pragma once

#ifndef STARTING_FORCE_H
#define STARTING_FORCE_H

#include "Component.h"
#include "RigidBody.h"

class StartingForce : public Component
{
public:
	StartingForce() {}
	explicit StartingForce( const Vector2& force ) : mForce{ force } {}

	void Start()
	{
		if ( !HasComponent<RigidBody>() ) return;

		RigidBody& rb = GetComponent<RigidBody>();
		rb.AddForce( mForce );
	}

	bool Load( const rapidjson::Value& v )
	{
		if ( !v.HasMember( "StartingForce" ) )
			return false;

		const rapidjson::Value& s = v["StartingForce"];
		
		mForce = Vector2( s["X"].GetDouble(), s["Y"].GetDouble() );

		return true;
	}

protected:
	Vector2 mForce;
};

#endif /* RANDOM_STARTING_FORCE_H */