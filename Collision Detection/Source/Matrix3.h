// Jake

#pragma once

#ifndef MATRIX_3
#define MATRIX_3

#include "Vector3.h"

class Matrix3
{
public:
	Matrix3( float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz );
	Matrix3( const Vector3 &_x, const Vector3 &_y, const Vector3 &_z );
	
	Vector3 &operator [] ( int i );
	const Vector3 &operator [] ( int i ) const;

	Vector3 x;
	Vector3 y;
	Vector3 z;
};

#endif /* MATRIX_3 */