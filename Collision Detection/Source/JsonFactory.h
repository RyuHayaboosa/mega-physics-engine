// Jake

#pragma once

#ifndef JSON_FACTORY_H
#define JSON_FACTORY_H

#include <string>
#include <vector>

#include "rapidjson/document.h"

struct SimParameters;

class SimObject;

class JsonFactory
{
public:
	static void Parse( const char* const filename );

	static std::vector<SimObject *> *CreateSimObjects();
	static SimParameters *CreateSimParameters();
	
private:
	static std::string			mFilename;
	static std::string			mFile;
	static rapidjson::Document	mDocument;

	template<typename T>
	static void AddComponent( const rapidjson::Value& v, const std::vector<SimObject*>& metaObjects, int index, int meta = -1 )
	{
		// try on original call only
		if ( meta < 0 )
		{
			// load component 
			T temp;
			if ( temp.Load( v[index] ) )
			{
				T& component = metaObjects[index]->AddComponent<T>();
				component.Load( v[index] );
			}

			// get original objects meta
			meta = metaObjects[index]->GetMeta();
		}
		else
		{
			// get metas meta
			meta = metaObjects[meta]->GetMeta();
		}

		// return if the component was never meant to be
		if ( meta < 0 )
			return;

		// add if meta had component
		if ( metaObjects[meta]->HasComponent<T>() )
		{
			T& component = metaObjects[index]->AddComponent<T>();
			component.Load( v[meta] );
		}
		// try metas meta
		else
		{
			AddComponent<T>( v, metaObjects, index, meta );
		}
	}
};

#endif /* JSON_FACTORY_H */