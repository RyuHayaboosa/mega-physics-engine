#include "Shape.h"

#include <string>

bool Shape::Load( const rapidjson::Value& v )
{
	if ( !v.HasMember( "Shape" ) )
		return false;

	std::string s = v["Shape"].GetString();

	if		( s == "Cube" )		topology = Shape::cube;
	else if ( s == "Sphere" )	topology = Shape::sphere;

	return true;
}