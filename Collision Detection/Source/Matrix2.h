// Jake

#pragma once

#ifndef MATRIX_2
#define MATRIX_2

#include "Vector2.h"

class Matrix2
{
public:
	// Default Constructors
	Matrix2( float xx = 1.0f, float xy = 0.0f, float yx = 0.0f, float yy = 1.0f );
	Matrix2( const Vector2 &_x, const Vector2 &_y );

	// Other constructors
	explicit Matrix2( float rotation );

	// Altering functions
	Matrix2 &operator *= ( const Matrix2 &val );

	// Non-altering functions
	Matrix2 operator * ( const Matrix2 &val ) const;
	Vector2 operator * ( const Vector2 &val ) const;
	
	Vector2 &operator [] ( int i );
	const Vector2 &operator [] ( int i ) const;

	// Variables
	Vector2 x;
	Vector2 y;
};

#endif /* MATRIX_2 */