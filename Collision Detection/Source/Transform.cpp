#include "Transform.h"

Transform::Transform( const Vector2& _position, float _rotation, const Vector2& _scale ) :	position{ _position }, rotation{ _rotation }, scale{ _scale }
{

}

bool Transform::Load( const rapidjson::Value& v )
{
	if ( !v.HasMember( "Transform" ) )
		return false;

	const rapidjson::Value& t = v["Transform"];
	
	position = Vector2( t["Position"]["X"].GetDouble(), t["Position"]["Y"].GetDouble() );
	rotation = t["Rotation"].GetDouble();
	scale = Vector2( t["Scale"]["X"].GetDouble(), t["Scale"]["Y"].GetDouble() );

	return true;
}