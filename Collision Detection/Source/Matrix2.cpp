#include "Matrix2.h"

#include <cmath>

Matrix2::Matrix2( float xx, float xy, float yx, float yy ) : x{ xx, xy }, y{ yx, yy }
{

}

Matrix2::Matrix2( const Vector2 &_x, const Vector2 &_y ) : Matrix2{ _x.x, _x.y, _y.x, _y.y }
{

}

Matrix2::Matrix2( float rotation ) : Matrix2{ std::cosf( rotation ), -std::sinf( rotation ), std::sinf( rotation ), std::cosf( rotation ) }
{

}

Matrix2 &Matrix2::operator *= ( const Matrix2 &val )
{
	Matrix2 retval;

	for ( int i = 0; i < 2; i++ )
	{
		for ( int j = 0; j < 2; j++ )
		{
			for ( int k = 0; k < 2; k++ )
			{
				retval[i][j] += ( *this )[i][k] * val[k][j];
			}
		}
	}
	
	this->x = retval.x;
	this->y = retval.y;

	return *this;
}

Matrix2 Matrix2::operator * ( const Matrix2 &val ) const
{
	Matrix2 retval{ *this };
	retval *= val;
	return retval;
}

Vector2 Matrix2::operator * ( const Vector2 &val ) const
{
	return { x.x * val.x + x.y * val.y, y.x * val.x + y.y * val.y };
}

Vector2 &Matrix2::operator [] ( int i )
{
	switch ( i )
	{
	case 0: return x;
	case 1: return y;

	default:
		throw "Matrix2::operator[int] subscript out of range.";
	}
}

const Vector2 &Matrix2::operator [] ( int i ) const
{
	switch ( i )
	{
	case 0: return x;
	case 1: return y;

	default:
		throw "Matrix2::operator[int] (const) subscript out of range.";
	}
}