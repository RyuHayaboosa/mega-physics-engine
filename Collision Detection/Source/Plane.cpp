#include "Plane.h"

#include <cfloat>
#include <cmath>

Plane::Plane( const Vector2 &n, float d ) : BoundingVolume{ BoundingVolume::plane }, normal{ n }, distance{ d }
{
	normal.Normalise();
}

Vector2 Plane::ClosestPointOnPlane( const Vector2 &p )
{
	return p - normal * ( Vector2::Dot( normal, p ) - distance );
}

Vector2 Plane::GetTranslation() const
{
	// closest point to origin
	return normal * distance;
}

float Plane::GetRotation() const
{
	// returns in radians
	return std::atan2f( normal.y, normal.x ) * 180.0f / 3.14159f;
}

Vector2 Plane::GetScale() const
{
	// no concept of scale for an infinite plane, just return a big number
	return{ FLT_MAX, FLT_MAX };
}

void Plane::SetTranslation( const Vector2 &t )
{
	distance = Vector2::Dot( normal, t );
}

void Plane::SetRotation( float r )
{
	r *= 3.14159f / 180.0f;

	// assumes radians
	normal.x = std::cosf( r );
	normal.y = std::sinf( r );
	normal.Normalise();
}

void Plane::SetScale( const Vector2 &s )
{
	// Planes cant scale
}

void Plane::Transform( const Vector2 &t, float r, const Vector2 &s )
{
	SetRotation( r );
	SetTranslation( t );
}