// Jake

#pragma once

#ifndef COMPONENT_H
#define COMPONENT_H

#include "JsonLoadable.h"
#include "SimBehaviour.h"
#include "SimObject.h"

class Component : public SimBehaviour, public JsonLoadable
{
public:
	// setters
	void SetOwner( SimObject *so ) { mSimObject = so; }

	// behaviour
	virtual void Start()			{}
	virtual void Update()			{}
	virtual void LateUpdate()		{}
	virtual void Render( float t )	{}
	
protected:
	template<typename T>
	T& GetComponent() const
	{
		return mSimObject->GetComponent<T>();
	}

	template<typename T>
	bool HasComponent() const
	{
		return mSimObject->HasComponent<T>();
	}

	SimObject *mSimObject;
};

#endif /* COMPONENT_H */