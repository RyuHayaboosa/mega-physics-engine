#include "Timer.h"

using namespace std::chrono;

const double Timer::MsPerUpdate = 1.0 / 60.0;

Timer::Timer()
{
	Start();
}

void Timer::Start()
{
	mStart = steady_clock::now();
}

void Timer::Reset()
{
	Start();
}

double Timer::ElapsedTime() const 
{
	return duration_cast<duration<double>>( steady_clock::now() - mStart ).count();
}