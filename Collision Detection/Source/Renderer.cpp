#include "Renderer.h"

#include "Material.h"
#include "Shape.h"
#include "Transform.h"

#include "Vector3.h"

#include "Graphics.h"

void Renderer::Render( float t )
{
	Material&	m = GetComponent<Material>();
	Shape&		s = GetComponent<Shape>();

	// interpolate between previous and current frame
	Vector2 position	= ( mSimObject->transform->position	* t ) + ( oldTransform.position	* ( 1.0f - t ) );
	float rotation		= ( mSimObject->transform->rotation	* t ) + ( oldTransform.rotation	* ( 1.0f - t ) );
	Vector2 scale		= ( mSimObject->transform->scale	* t ) + ( oldTransform.scale	* ( 1.0f - t ) );

	// Store old frames transform
	oldTransform = *mSimObject->transform;

	switch ( s.topology )
	{
	case Shape::cube:
		Graphics::DrawCube( position, rotation, scale, { m.r, m.g, m.b } );
		break;
	case Shape::sphere:
		Graphics::DrawSphere( position, rotation, scale, { m.r, m.g, m.b } );
		break;
	}
}

bool Renderer::Load( const rapidjson::Value& v )
{
	return v.HasMember( "Renderer" ) && v["Renderer"].GetBool();
}