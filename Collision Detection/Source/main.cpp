﻿// Jake

#include "JsonFactory.h"
#include "RuntimeData.h"
#include "Simulation.h"

int main( int argc, char *argv[] )
{
	// Watch data
	RuntimeData::Init();

	// Load json file
	JsonFactory::Parse( "../Simulation.json" );
	
	// Run simulation
	Simulation::Init( argc, argv, JsonFactory::CreateSimParameters(), JsonFactory::CreateSimObjects() );
	Simulation::Start();
	Simulation::Stop();
	
	// Save data
	RuntimeData::SaveToFile("../Data.json");
	
	return 0;
}