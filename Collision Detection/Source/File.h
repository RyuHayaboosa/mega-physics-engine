// Jake

#ifndef FILE_H
#define FILE_H

#include <string>

namespace File
{
	std::string Read( const std::string &filename );
}

#endif