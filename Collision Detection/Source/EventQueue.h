// Jake

#pragma once

#ifndef EVENT_QUEUE_H
#define EVENT_QUEUE_H

#include <vector>

enum Event { sim_objects_loaded, collision, sim_object_velocity_updated, frame_completed, last };

class EventQueue
{
public:
	static void Listen( Event e, void( *)( void *pData ) );
	static void Notify( Event e, void *pData );

private:
	static std::vector<void( *)( void *pData )> mCallbacks[Event::last];
};

#endif /* EVENT_QUEUE_H */