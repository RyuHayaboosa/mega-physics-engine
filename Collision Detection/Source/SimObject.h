// Jake

#pragma once

#ifndef SIM_OBJECT_H
#define SIM_OBJECT_H

#include <map>
#include <string>
#include <typeinfo>

#include "SimBehaviour.h"

class Collider;
class Component;
class RigidBody;
class Transform;

class SimObject : public SimBehaviour
{
public:
	explicit SimObject( int meta, bool useSpatialPartitioning );
	
	// behaviour
	void Start();
	void Update();
	void LateUpdate();
	void Render( float t );
	
	template<typename T>
	T& AddComponent()
	{
		T* component = new T();

		component->SetOwner( this );
		mComponents.insert( std::pair<std::string, Component*>( typeid( T ).name(), component ) );

		return *component;
	}
	
	template<typename T>
	T& GetComponent() const
	{
		auto i = mComponents.find( typeid( T ).name() );
		if ( i != mComponents.end() )
			return *(T*)(i->second);
		else
			throw "SimObject::GetComponent(" + (std::string)typeid( T ).name() + ") failed.\n";
	}
	
	template<typename T>
	bool HasComponent() const
	{
		std::string key = typeid( T ).name();

		return mComponents.find( key ) != mComponents.end();
	}

	template<typename T>
	bool RemoveComponent()
	{
		auto i = mComponents.find( typeid( T ).name() );
		if ( i != mComponents.end() )
		{
			mComponents.erase( i );
			return true;
		}
		else
		{
			std::cout << "SimObject::RemoveComponent(" << typeid( T ).name() << ") failed.\n";
			return false;
		}
	}

	// meta
	int		GetMeta() const		{ return mMeta; }
	bool	HasMeta() const		{ return mMeta > -1; }

	// spatial partition
	int		GetSpatialPartition() const	{ return mSpatialPartition; }
	bool	UseSpatialPartition() const { return mUseSpatialPartitioning; }
	void	SetSpatialPartition( int p ) { mSpatialPartition = p; }

	// quick access components (most sim objects have them)
	Collider *collider;
	RigidBody *rigidBody;
	Transform *transform;

private:
	std::map<std::string, Component*> mComponents;
	int mMeta;

	bool mUseSpatialPartitioning;
	int mSpatialPartition;
};

#endif /* SIM_OBJECT_H */