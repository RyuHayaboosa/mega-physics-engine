#include "SpatialPartitionGrid.h"

#include "Graphics.h"
#include "SimObject.h"
#include "Transform.h"
#include "Vector3.h"

SpatialPartitionGrid::Partition SpatialPartitionGrid::mPartitions[SpatialPartitionGrid::Width][SpatialPartitionGrid::Height];	// lol, what?

void SpatialPartitionGrid::Init()
{
	const float range = Graphics::GetRange();
	const float psize = range * 0.5f;
	const float ratio = (float)Graphics::GetWindowWidth() / Graphics::GetWindowHeight();

	// initialise partitions
	for ( int i = 0; i < Width; i++ )
	{
		for ( int j = 0; j < Height; j++ )
		{
			mPartitions[i][j].id = ( i * Width ) + j;
			mPartitions[i][j].min = { psize * i - range, psize * j - range };
			mPartitions[i][j].max = mPartitions[i][j].min + Vector2{ psize, psize };	// implicit constructor doesnt work here!
		}
	}
}

bool SpatialPartitionGrid::PlaceSimObject( SimObject &so )
{
	// check each partition starting from top left
	// no need to check min boundaries if we start from top left

	int i = 0, j = 0;
	while ( i < Width || j < Height )
	{
		int p = i + j;

		// if sim object is to the right of the partition, increment
		if ( so.transform->position.x > mPartitions[i][j].max.x ) i++;
		
		// if sim object is below the partition, increment
		if ( so.transform->position.y > mPartitions[i][j].max.y ) j++;

		// if no increment (we're in the partition)
		if ( p == i + j )
		{
			so.SetSpatialPartition( mPartitions[i][j].id );
			return true;
		}
	}

	return false;
}

bool SpatialPartitionGrid::DetectAdjacency( const SimObject &a, const SimObject &b )
{
	// stop, if at least one sim object doesnt use spatial partitioning
	if ( !(a.UseSpatialPartition() && b.UseSpatialPartition()) ) return true;

	int ap = a.GetSpatialPartition();
	int bp = b.GetSpatialPartition();
	
	// same partition
	bool adjacent = ( ap == bp );

	// to the left or right
	adjacent |= ( ap > 0 ) && ( ap - 1 == bp );
	adjacent |= ( bp > 0 ) && ( ap == bp - 1 );
	
	// above or below
	adjacent |= ( ap >= Width ) && ( ap - Width == bp );
	adjacent |= ( bp >= Width ) && ( ap == bp - Width );

	return adjacent;
}

void SpatialPartitionGrid::Render()
{
	for ( int i = 0; i < Width; i++ )
	{
		for ( int j = 0; j < Height; j++ )
		{
			Vector2 position = mPartitions[i][j].max - ( mPartitions[i][j].max - mPartitions[i][j].min ) * 0.5f;

			Graphics::DrawCube( position, 0.0f, mPartitions[i][j].max - mPartitions[i][j].min, { 0.0f, 1.f, 1.0f }, true );
		}
	}
}