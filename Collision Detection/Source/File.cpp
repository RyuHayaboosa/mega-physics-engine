#include "File.h"

#include <fstream>
#include <iostream>
#include <sstream>

std::string File::Read( const std::string &filename )
{
	std::ifstream t{ filename };

	if ( t.is_open() )
	{
		std::cout << "File::Read() successful.\n";

		std::stringstream buffer;
		buffer << t.rdbuf();

		return buffer.str();
	}
	else
	{
		std::cout << "File::Read() failed.\n";
		return "";
	}
}