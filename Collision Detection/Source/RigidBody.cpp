#include "RigidBody.h"

#include "Transform.h"
#include "Timer.h"
#include "EventQueue.h"

RigidBody::RigidBody()
{

}

RigidBody::RigidBody( float mass, float restitution ) : mInverseMass{ 1.0f / mass }, mRestitution{ restitution }
{

}

void RigidBody::AddForce( const Vector2 &force, bool biased )
{
	if ( biased )
		mForce += force * ForceCoeff;
	else
		mForce += force;
}

void RigidBody::AddVelocity( const Vector2 &velocity )
{
	mVelocity += velocity;
}

void RigidBody::Update()
{
	/*
		Position
	*/

	// a = f / m
	mAcceleration = mForce * mInverseMass;
	mForce.Zero();

	// gravity
	mAcceleration += { 0.0f, 9.81f };

	// v = at
	mVelocity += mAcceleration * Timer::MsPerUpdate;

	// s = vt
	mSimObject->transform->position += mVelocity * Timer::MsPerUpdate;

	/*
		And now for something completely different...
	*/

	float energy = ( mVelocity * ( 1.0f / mInverseMass ) ).Length();
	EventQueue::Notify( Event::sim_object_velocity_updated, &energy );
}

bool RigidBody::Load( const rapidjson::Value& v )
{
	if ( !v.HasMember( "RigidBody" ) )
		return false;

	const rapidjson::Value& b = v["RigidBody"];

	mInverseMass	= 1.0f / b["Mass"].GetDouble();
	mRestitution	= b["Restitution"].GetDouble();

	return true;
}