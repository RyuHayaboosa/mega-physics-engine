// Jake

#pragma once

#ifndef VECTOR_2_H
#define VECTOR_2_H

class Vector3;

class Vector2
{
public:
	Vector2();
	Vector2( float _x, float _y );
	explicit Vector2( const Vector3 &v );

	// Altering functions
	Vector2 &operator = ( const Vector2 &val );
	Vector2 &operator += ( const Vector2 &val );
	Vector2 &operator -= ( const Vector2 &val );
	Vector2 &operator *= ( const Vector2 &val );

	// Non-altering functions
	Vector2 operator + ( ) const;
	Vector2 operator - ( ) const;
	Vector2 operator + ( const Vector2 &val ) const;
	Vector2 operator - ( const Vector2 &val ) const;
	Vector2 operator * ( const Vector2 &val ) const;
	Vector2 operator / ( float val ) const;
	Vector2 operator * ( float val ) const;
	
	float &operator [] ( int i );
	const float &operator [] ( int i ) const;
	
	// Help
	void	Abs();
	void	Clamp( const Vector2 &min, const Vector2 &max );
	void	Inverse();
	float	Length() const;
	float	LengthSq() const;
	void	Normalise();
	void	Perpendicular();
	void	Zero();
	
	// Static help
	static Vector2 Abs( const Vector2 &a );
	static Vector2 Clamp( const Vector2 &val, const Vector2 &min, const Vector2 &max );
	static float Dot( const Vector2 &a, const Vector2 &b );

	// Variables
	float x = 0.0f, y = 0.0f;
};

#endif /* VECTOR_2_H */