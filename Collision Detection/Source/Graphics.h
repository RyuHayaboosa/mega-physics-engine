// Jake

#pragma once

#ifndef GRAPHICS_H
#define GRAPHICS_H

class Frustum;
class Vector2;
class Vector3;

class Graphics
{
public:
	// constructor / deconstructor
	static void Init( int argc, char *argv[], int width, int height, void( *display )( ), void( *resize )( int, int ), const char *const title );
	static void Stop();
	
	// callbacks
	static void Resize( int w, int h );

	// simulation behaviour
	static void Start();

	// defines single framebuffer
	static void BeginDraw();
	static void EndDraw();
	
	// draw primitives between begin / end 
	static void DrawCube	( const Vector2 &t, float r, const Vector2 &s, const Vector3 &c, bool wireframe = false );
	static void DrawSphere	( const Vector2 &t, float r, const Vector2 &s, const Vector3 &c, bool wireframe = false );
	static void DrawPlane	( const Vector2 &t, float r, const Vector2 &s, const Vector3 &c, bool wireframe = false );
	
	// getters
	static int GetWindowWidth();
	static int GetWindowHeight();
	static float GetRange();

private:
	static Frustum *mFrustum;
};

#endif /* GRAPHICS_H */