#include "Vector3.h"

#include <cmath>

Vector3::Vector3()
{

}

Vector3::Vector3( float _x, float _y, float _z ) : x{ _x }, y{ _y }, z{ _z }
{

}

Vector3 &Vector3::operator = ( const Vector3 &val )
{
	x = val.x;
	y = val.y;
	z = val.z;
	return *this;
}

Vector3 Vector3::operator + ( const Vector3 &val ) const
{
	return Vector3{ x + val.x, y + val.y, z + val.z };
}

Vector3 Vector3::operator - ( const Vector3 &val ) const
{
	return Vector3{ x - val.x, y - val.y, z - val.z };
}

Vector3 Vector3::operator * ( float f ) const
{
	return Vector3{ x * f, y * f, z * f };
}

float &Vector3::operator [] ( int i )
{
	switch ( i )
	{
	case 0:	return x;
	case 1: return y;
	case 2: return z;

	default: 
		throw "Vector3::operator[int] subscript out of range.";
	}
}

const float &Vector3::operator [] ( int i ) const
{
	switch ( i )
	{
	case 0:	return x;
	case 1: return y;
	case 2: return z;

	default: 
		throw "Vector3::operator[int] (const) subscript out of range.";
	}
}

void Vector3::Inverse()
{
	x = 1.0f / x;
	y = 1.0f / y;
	z = 1.0f / z;
}

float Vector3::Length() const
{
	return sqrtf( ( x * x ) + ( y * y ) + ( z * z ) );
}

void Vector3::Normalise()
{
	float length = Length();

	x /= length;
	y /= length;
	z /= length;
}

void Vector3::Zero()
{
	x = y = z = 0.0f;
}

float Vector3::Dot( const Vector3& a, const Vector3& b )
{
	return ( a.x * b.x ) + ( a.y * b.y ) + ( a.z * b.z );
}

Vector3 Vector3::Cross( const Vector3& a, const Vector3& b )
{
	return Vector3( 
		( a.y * b.z ) - ( a.z * b.y ), 
		( a.x * b.z ) - ( a.z * b.x ), 
		( a.x * b.y ) - ( a.y * b.x ) 
	);
}