// Jake

#pragma once

#ifndef CONSOLE_H
#define CONSOLE_H

class Console
{
public:
	enum Colour { Red = 4, Blue = 2, Green = 1 };

public:
	static void Init();
	static void Unin();

	static void Print( const char *const message );
	static void Print( const char *const message, int x, int y );
	static void Print( const char *const message, Colour c );
	static void Print( const char *const message, int x, int y, Colour c );
	
	static void ClearScreen();
	static void GotoXY( int x, int y );
	static void SetColour( Colour c );
};

#endif /* CONSOLE_H */