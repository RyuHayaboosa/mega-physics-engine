// Jake

#pragma once

#ifndef SHAPE_H
#define SHAPE_H

#include "Component.h"

class Shape : public Component
{
public:
	enum { cube, sphere } topology;

	bool Load( const rapidjson::Value& v );
};

#endif /* SHAPE_H */