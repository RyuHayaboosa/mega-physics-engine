// Jake

#pragma once

#ifndef RIGID_BODY_H
#define RIGID_BODY_H

#include "Component.h"
#include "Vector2.h"

class RigidBody : public Component
{
public:
	RigidBody();
	explicit RigidBody( float mass, float restitution );
	
	// getters
	const Vector2 &GetVelocity() const	{ return mVelocity; }
	float GetInverseMass() const		{ return mInverseMass; }
	float GetRestitution() const		{ return mRestitution; }

	// adders
	void AddForce( const Vector2 &force, bool biased = true );
	void AddVelocity( const Vector2 &velocity );

	// inherited
	void Update();

	bool Load( const rapidjson::Value& v );

private:
	// Position
	Vector2 mVelocity;
	Vector2 mAcceleration;
	Vector2 mForce;
	float	mInverseMass;

	// Rotation
	Vector2 mAngularVelocity;
	Vector2 mAngularAcceleration;
	Vector2 mTorque;
	float	mInverseInertia;
	
	// Other
	float mRestitution;
	
	const unsigned int ForceCoeff = 1000;
};

#endif /* RIGID_BODY_H */